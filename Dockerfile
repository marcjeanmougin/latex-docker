FROM ubuntu:latest

# fu tzdata
ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get -yqq update
RUN apt-get install -yqq texlive-full biber make icc-profiles python3 python3-pip python3-coverage
